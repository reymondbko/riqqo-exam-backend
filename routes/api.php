<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], function () {
    
    Route::post('login', 'Auth\ApiAuthController@login');
  
    Route::group(['middleware' => 'auth.api'], function() {
        Route::get('user', 'ApiController@user');
        Route::post('logout', 'Auth\ApiAuthController@logout');
        Route::get('user/customers', 'ApiController@customers');
        Route::post('user/fetchcustomer', 'ApiController@fetchcustomer');
        Route::post('user/addcustomer', 'ApiController@addcustomer');
        Route::post('user/editcustomer', 'ApiController@editcustomer');
        Route::post('user/deletecustomer', 'ApiController@deletecustomer');
    });
});