<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Cartalyst\Stripe\Stripe;

class ApiController extends Controller
{
    //fetch user
    public function user(Request $request)
    {
        return response()->json($request->user());  
    }
    
    //fetch customers
    public function customers(Request $request)
    {
        try{
            $stripe = new Stripe(env('STRIPE_API_KEY'));
            $data = $stripe->customers()->all();
            return response()->json(['error'=>false,'data'=>$data]);
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }        
    }
    //fetch single customer
    public function fetchcustomer(Request $request)
    {
        try{
            $stripe = new Stripe(env('STRIPE_API_KEY'));
            $data = $stripe->customers()->find($request->customerid);
            return response()->json(['error'=>false,'data'=>$data]);
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }        
    }
    //add new customers to stripe
    public function addcustomer(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:200',
                'email' => 'required|string|email'
                ]);
            if($validator->fails()){
                return response()->json(['error'=>true,'data'=>$validator->errors(),'msg'=>'Validation errors.']);
            }else{
                $stripe = new Stripe(env('STRIPE_API_KEY'));
                
                $customer = $stripe->customers()->create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                ]);
                $data = $customer;
                 return response()->json(['error'=>false,'data'=>$data]);
            }

            /*
            'shipping' => [
                'name'    => 'Jenny Rosen',
                'address' => [
                    'line1'       => '1234 Main street',
                    'city'        => 'Anytown',
                    'country'     => 'US',
                    'postal_code' => '123456',
                ],
            ],
            */            
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }
    }

    //update customer
    public function editcustomer(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:200',
                'email' => 'required|string|email'
                ]);
            if($validator->fails()){
                return response()->json(['error'=>true,'data'=>$validator->errors(),'msg'=>'Validation errors.']);
            }else{
                $stripe = new Stripe(env('STRIPE_API_KEY'));
                $customer = $stripe->customers()->update($request->id,[
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                ]);
                $data =$customer;
                return response()->json(['error'=>false,'data'=>$data]);
            }
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }        
    }

    //update customer
    public function deletecustomer(Request $request)
    {
        try{
            $stripe = new Stripe(env('STRIPE_API_KEY'));
            $customer = $stripe->customers()->delete($request->customerid);
            return response()->json(['error'=>false,'data'=>"Customer Deleted"]);
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }        
    }

    


}
